package readinglist.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import readinglist.domain.Book;
import readinglist.domain.ReadingList;

import java.util.List;

@RepositoryRestResource
public interface ReadingListRepository extends PagingAndSortingRepository<ReadingList, Long> {
	
	@Query("select rl from ReadingList rl join fetch rl.reader join fetch rl.book where rl.book = ?1 ")
	List<ReadingList> findByBook(Book book);

}