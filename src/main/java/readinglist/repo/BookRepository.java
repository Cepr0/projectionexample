package readinglist.repo;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import readinglist.domain.Book;
import readinglist.rest.BookWithRating;

import java.util.List;

@RepositoryRestResource
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    @RestResource(exported = false)
	@Query("select avg(rl.rating) as bookRating, b as book from ReadingList rl join rl.book b group by rl.book order by bookRating desc")
	List<BookWithRating> findAllWithRating(Pageable pageable);
}
