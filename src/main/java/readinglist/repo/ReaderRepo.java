package readinglist.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import readinglist.domain.Reader;

/**
 * @author Cepro, 2017-03-24
 */
@RepositoryRestResource
public interface ReaderRepo extends CrudRepository<Reader, String> {
}
