package readinglist.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Data
@NoArgsConstructor
@Entity
public class Reader  {

	@Id @GeneratedValue
    private Long id;
	
    private String username;
    private String fullName;
    
    @JsonProperty(access = WRITE_ONLY)
    private String password;
    
    public Reader(String username, String fullName, String password) {
        this.username = username;
        this.fullName = fullName;
        this.password = password;
    }
}
