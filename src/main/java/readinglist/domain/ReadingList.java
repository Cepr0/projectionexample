package readinglist.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class ReadingList {
    
    @Id
    @GeneratedValue
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "reader")
    private Reader reader;
    
    @ManyToOne
    @JoinColumn(name = "book")
    private Book book;
    
    private String review;
    private Double rating;
    
    public ReadingList(Reader reader, Book book, String review, double rating) {
        
        this.reader = reader;
        this.book = book;
        this.review = review;
        this.rating = rating;
    }
}
