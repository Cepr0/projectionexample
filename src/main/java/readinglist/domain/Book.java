package readinglist.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class Book {

    @Id
	@GeneratedValue
    private Long id;
    
    private String isbn10;
	private String isbn13;
    private String title;
    private String author;

    public Book(String isbn10, String isbn13, String title, String author) {

        this.isbn10 = isbn10;
        this.isbn13 = isbn13;
        this.title = title;
        this.author = author;
    }
}
