package readinglist.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import readinglist.repo.BookRepository;

import java.util.List;


@RequiredArgsConstructor
@RepositoryRestController
@RequestMapping("/books")
class BookController {

	private final BookRepository bookRepo;

    @GetMapping("/rating")
    public ResponseEntity<?> getWithRating(Pageable pageable) {
        List<BookWithRating> books = bookRepo.findAllWithRating(pageable);
        return ResponseEntity.ok(books);
    }
}