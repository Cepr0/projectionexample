package readinglist.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.rest.core.config.Projection;
import readinglist.domain.Book;

/**
 * @author Cepro, 2017-03-24
 */
@Projection(name = "BookWithRating", types = { Book.class })
public interface BookWithRating {

    @JsonProperty("rating")
    Double getBookRating();
    Book getBook();
}
