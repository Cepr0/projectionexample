package readinglist;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import readinglist.domain.Book;
import readinglist.domain.Reader;
import readinglist.domain.ReadingList;
import readinglist.repo.BookRepository;
import readinglist.repo.ReaderRepo;
import readinglist.repo.ReadingListRepository;

import static java.util.Arrays.asList;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

    @Bean
    public CommandLineRunner demo(BookRepository bookRepo, ReaderRepo readerRepo, ReadingListRepository listRepo) {
        return args -> {

            Book book1 = new Book("1", "1", "Title1", "Author1");
            Book book2 = new Book("2", "2", "Title2", "Author2");
            Book book3 = new Book("3", "3", "Title3", "Author3");
            bookRepo.save(asList(book1, book2, book3));

            Reader reader1 = new Reader("User1", "Name1", "123456");
            Reader reader2 = new Reader("User2", "Name2", "123456");
            readerRepo.save(asList(reader1, reader2));

            listRepo.save(asList(
                    new ReadingList(reader1, book1, "review11", 1.0),
                    new ReadingList(reader1, book2, "review12", 2.0),
                    new ReadingList(reader1, book3, "review13", 3.0),
                    new ReadingList(reader2, book1, "review21", 4.0),
                    new ReadingList(reader2, book2, "review22", 5.0),
                    new ReadingList(reader2, book3, "review23", 6.0)
            ));
        };
    }
}